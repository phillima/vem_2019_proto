﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AVisualizer : MonoBehaviour
{
    IParser parser = new XMLParser();
    //SpawnManager spawnManager;

    public ProjectContainer ProjectContainer { get; set; }
    public Dictionary<string,Color> AnnotSchemaColor { get; set; }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        //Parses de XML
        ProjectContainer = parser.Parse(null);
        AnnotSchemaColor = new Dictionary<string, Color>();
        //Initiates drawing process
        //spawnManager = FindObjectOfType<SpawnManager>();
        //spawnManager.SpawnProject(projectContainer);
    }
}