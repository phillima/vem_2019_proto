﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    const string FLOATING_TEXT_LAYER = "Floating_Text";
    // Start is called before the first frame update
    void Start()
    {
        TextMesh textMesh = GetComponent<TextMesh>();
        textMesh.text = transform.parent.name;
        gameObject.GetComponent<MeshRenderer>().sortingLayerName = FLOATING_TEXT_LAYER;
    }

    
}
