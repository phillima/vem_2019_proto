﻿using System.Collections.Generic;

public class AnnotationContainer
{
    public string Name { get; set; }
    public string Schema { get; set; }

    public Dictionary<string,int> Metrics { get; set; }
}
