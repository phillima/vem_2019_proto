﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class that represents a Java Class, with name, LOC and annotation metrics
/// </summary>
public class ClassContainer : IComparable<ClassContainer> {
    public string Name { get; set; }
    public string FilePath { get; set; }
    public string Type { get; set; }
    public float CHN { get; set; }

    /// <summary>
    /// Class metrics
    /// Currently we have AC, ASC, UAC, LOC, NAEC, NEC
    /// </summary>
    public Dictionary<string, int> ClassMetrics { get; set; }

    /// <summary>
    /// Element Containers
    /// methods, fields, enums, types, interfaces
    /// </summary>
    public Dictionary<string, ElementContainer> ElementCointainers {get; set;}

    public int CompareTo(ClassContainer classContainer) {

        float classWidth = (float)ClassMetrics["LOC"] / CHN;

        float otherClassWidth = (float)classContainer.ClassMetrics["LOC"] / CHN;

        return otherClassWidth.CompareTo(classWidth);
    }
}