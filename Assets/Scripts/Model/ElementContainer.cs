﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SearchMaxValues()]
public class ElementContainer : IComparable<ElementContainer>
{
    public string Name { get; set; }
    public string Type { get; set; } //Method, field, class, enum, interface

    public List<AnnotationContainer> Annotations { get; set; } //annotation name, value
    public int AED { get; set; }

    public int CompareTo(ElementContainer otherElementContainer) {

        //Simple hack to guarante that interface comes before fields 
        // in the drawing process
        string thisInterface = Type, otherInterface = otherElementContainer.Type;
        if (Type.Equals("interface"))
            thisInterface = "dInterface";
        if (otherElementContainer.Type.Equals("interface"))
            otherInterface = "dInterface";

        return thisInterface.CompareTo(otherInterface);
    }
}
