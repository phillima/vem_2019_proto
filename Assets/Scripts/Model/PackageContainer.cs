﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This class represents information of a Java package, such as name and its classes
/// </summary>
public class PackageContainer : MonoBehaviour
{
    /// <summary>
    /// The name of the package
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Classes elements and its metrics values for this package
    /// </summary>
    public List<ClassContainer> ClassContainer { get; set; } = new List<ClassContainer>();
}
