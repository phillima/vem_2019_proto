﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectContainer
{
    public string Name { get; set; } //Project Name
    public float CHN { get; private set; } //Class Height Normalization
    public int Max_Aed { get; private set; } = 0;
    public int Max_Loc { get; private set; } = int.MaxValue;
    public HashSet<string> Schemas { get; set; } = new HashSet<string>(); // annotations schemas in the project

    private int _max;
    public int Max_AA {
        get { return _max; }
        set {
            if (value > _max)
                _max = value;
        }
    }

    //Contains the packageContainers of the project
    public List<PackageContainer> PackageContainers { get; set; } = new List<PackageContainer>();

    public void DefineCHN() {
        CHN = (float)Max_Loc / (float)Max_Aed;
    }
    /// <summary>
    /// Define the max parameter used for normalization
    /// </summary>
    /// <param name="maxAed">Maximum AED Value</param>
    /// <param name="maxLoc">Maximum LOC Value</param>
    public void SetMaxAED(int maxAed, int maxLoc) {
        if (maxAed > Max_Aed) {//If aed is greater update both maxAed and maxLoc
            Max_Aed = maxAed;
            Max_Loc = maxLoc;
        } else if (maxAed.Equals(Max_Aed)) //If aed is equal to current maxAed, 
            if (maxLoc < Max_Loc)         //update maxLoc only if it is smaller than current maxLoc
                Max_Loc = maxLoc;
    }
}
