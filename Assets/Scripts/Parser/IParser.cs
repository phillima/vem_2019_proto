﻿
public interface IParser 
{
    ProjectContainer Parse(string projectReportPath);
}
