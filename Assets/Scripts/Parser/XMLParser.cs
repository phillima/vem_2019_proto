﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;

public class XMLParser : MonoBehaviour, IParser {

    string xmlFilePath = Environment.CurrentDirectory + Path.DirectorySeparatorChar +
                        "Assets" + Path.DirectorySeparatorChar
                        + "Input" + Path.DirectorySeparatorChar + "vem.xml";
    ProjectContainer projectContainer;

    public ProjectContainer Parse(string projectReportPath) {

        projectContainer = new ProjectContainer();
        //Load in the XML document
        XElement javaProjectReportXML = XElement.Load(xmlFilePath);

        //Fetch project name
        var projectName = javaProjectReportXML.Attribute("name").Value;
        projectContainer.Name = projectName;

        //Fetch packages
        List<PackageContainer> packagesContainer = new List<PackageContainer>();
        IEnumerable<XElement> packagesXML = javaProjectReportXML.Elements();

        //Populate PackageContainer
        foreach (var packageXML in packagesXML) {
            PackageContainer packageContainer =
                    new PackageContainer { Name = packageXML.Attribute("name").Value };
            ParseClass(packageContainer, packageXML);
            packagesContainer.Add(packageContainer);
        }
        projectContainer.PackageContainers = packagesContainer;
        projectContainer.DefineCHN();

        return projectContainer;
    }

    void ParseClass(PackageContainer packageContainer, XElement packageXML) {

        int loc = 0;
        List<ClassContainer> classContainers = new List<ClassContainer>();
        IEnumerable<XElement> classesXML = packageXML.Elements();
        foreach (var clazzXML in classesXML) {
            ClassContainer classContainer = new ClassContainer {
                Name = clazzXML.Attribute("name").Value,
                Type = clazzXML.Attribute("type").Value,
                ClassMetrics = new Dictionary<string, int>()
            };
            //get all class metrics in XML
            IEnumerable<XElement> classMetricsXML = clazzXML.Elements("metric");

            foreach (var classMetric in classMetricsXML) {
                classContainer.ClassMetrics.Add(
                    classMetric.Attribute("name").Value,
                    int.Parse(classMetric.Attribute("value").Value)
                    );
                if (classMetric.Attribute("name").Value.Equals("loc"))
                    loc = int.Parse(classMetric.Attribute("value").Value);
            }

            //get all code elements
            IEnumerable<XElement> codeElementsXML = clazzXML.Elements("code-elements").
                                    Elements("code-element");
            
            ParseCodeElements(classContainer, codeElementsXML);

            classContainers.Add(classContainer);

            //Fetch schemas
            foreach (var schema in clazzXML.Elements("schema")){
                projectContainer.Schemas.Add(schema.Value); //save the schemas on the project container
            }
        }
        packageContainer.ClassContainer = classContainers;

        

    }

    void ParseCodeElements(ClassContainer classContainer, IEnumerable<XElement> codeElementsXML) {

        Dictionary<string, ElementContainer> elementContainers =
                                                new Dictionary<string, ElementContainer>();
        int maxClassAed = 0, loc = 0;
        foreach (var codeElement in codeElementsXML) {
            int aed = int.Parse(codeElement.Attribute("aed").Value);
            ElementContainer eleContainer = new ElementContainer { 
                Name = codeElement.Attribute("name").Value,
                Type = codeElement.Attribute("type").Value,
                AED = aed,
                Annotations = new List<AnnotationContainer>()
            };
            if (aed > maxClassAed) {
                maxClassAed = aed;
                loc = classContainer.ClassMetrics["LOC"];
            }
                
            //Get annotations in xml
            IEnumerable<XElement> annotationsXML = codeElement.Elements("annotation");                        

            ParseAnnotations(annotationsXML, eleContainer);

            elementContainers.Add(codeElement.Attribute("name").Value +
                                  "." +
                                  codeElement.Attribute("type").Value,
                                  eleContainer);
        }
        projectContainer.SetMaxAED(maxClassAed, loc);
        classContainer.ElementCointainers = elementContainers;
    }

    private void ParseAnnotations(IEnumerable<XElement> annotationsXML,
                                  ElementContainer eleCointainer) {

        foreach (var annotation in annotationsXML) {
            AnnotationContainer annotationContainer = new AnnotationContainer {
                Name = annotation.Attribute("name").Value,
                Metrics = new Dictionary<string, int>(),
                Schema = annotation.Attribute("schema").Value
            };
            IEnumerable<XElement> anotMetricsXML = annotation.Elements("annotation-metrics").Descendants();
            foreach (var annotationMetric in anotMetricsXML) {
                string metricName = annotationMetric.Attribute("metric").Value;
                int metricValue = int.Parse(annotationMetric.Attribute("value").Value);
                if (metricName.Equals("AA"))
                    projectContainer.Max_AA = metricValue;
                annotationContainer.Metrics.Add(
                                        metricName,
                                        metricValue);
            }
            eleCointainer.Annotations.Add(annotationContainer);
        }
    }
}