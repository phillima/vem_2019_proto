﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    const string TAG_SCHEMA_PANEL = "SchemaPanel";

    ProjectContainer projectContainer;
    HashSet<string> schemas;
    GameObject schemaPanel;
    [SerializeField]
    GameObject schemaUIPrefab;

    void Start()
    {
        schemaPanel = GameObject.FindGameObjectWithTag(TAG_SCHEMA_PANEL);
        projectContainer = FindObjectOfType<AVisualizer>().ProjectContainer;
        schemas = projectContainer.Schemas;
        ListSchemas();
    }

    void ListSchemas() {

        foreach (string schema in schemas) {
            GameObject schemaUIClone = Instantiate(schemaUIPrefab, schemaPanel.transform);
            schemaUIClone.GetComponentInChildren<Text>().text = schema;
        }
    }
}
