﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISchemaManager : MonoBehaviour
{

    Color rawImageColor;
    RawImage annotationPreviewImage;
    AVisualizer aVisualizer;
    string annotationName;
    Slider colorSlider;
    void Start()
    {
        aVisualizer = FindObjectOfType<AVisualizer>();
        annotationPreviewImage = GetComponentInChildren<RawImage>();
        annotationPreviewImage.color = Color.black;
        annotationName = GetComponentInChildren<Text>().text;
        foreach (Slider slider in GetComponentsInChildren<Slider>()) {
            slider.value = 0.0f;
        }
        aVisualizer.AnnotSchemaColor.Add(annotationName, Color.black);
    }

    public void OnSliderRChange(float value) {
        rawImageColor = annotationPreviewImage.color;
        annotationPreviewImage.color = new Color(value, rawImageColor.g, rawImageColor.b);
        aVisualizer.AnnotSchemaColor[annotationName] = annotationPreviewImage.color;
    }

    public void OnSliderGChange(float value) {
        rawImageColor = annotationPreviewImage.color;
        annotationPreviewImage.color = new Color(rawImageColor.r, value, rawImageColor.b);
        aVisualizer.AnnotSchemaColor[annotationName] = annotationPreviewImage.color;
    }

    public void OnSliderBChange(float value) {
        rawImageColor = annotationPreviewImage.color;
        annotationPreviewImage.color = new Color(rawImageColor.r, rawImageColor.g, value);
        aVisualizer.AnnotSchemaColor[annotationName] = annotationPreviewImage.color;
    }
}