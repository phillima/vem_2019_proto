﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnnotationPrefab : MonoBehaviour {

    [SerializeField]
    GameObject floatingText;

    AVisualizer aVisualizer;

    static Transform classParent = null;
    float offset = 1;

    static Vector3 annotPos = Vector3.zero;

    public static bool NewRow { get; set; } = true;
    public static int RowCounter { get; set; } = 0;
    public static int AnnotationCounter { get; set; } = 0;

    SpriteRenderer spriteRend;
    void Awake() {

        if (AnnotationCounter == 0)
            classParent = transform.parent;

        if (NewRow) {
            annotPos = classParent.position
                       + new Vector3(0.5f
                       + RowCounter, -0.5f, 0);
            NewRow = false;
        }

        spriteRend = GetComponent<SpriteRenderer>();

        transform.parent = null;
        aVisualizer = FindObjectOfType<AVisualizer>();
    }

    public void Configure(AnnotationContainer annotation, float maxAA) {
        float aa = annotation.Metrics["AA"];

        float diameter = (aa + 1) / (maxAA + 1) * 0.9f;

        transform.localScale = new Vector3(diameter, diameter, 0);
        //if (annotation.Name.Contains("Component") ||
        //    annotation.Name.Contains("Autowired"))
        //    spriteRend.color = Color.blue;

        spriteRend.color = aVisualizer.AnnotSchemaColor[annotation.Schema];

        transform.position = annotPos;
        annotPos += new Vector3(0, -offset, 0);
        transform.parent = classParent;
        AnnotationCounter++;
    }
}