﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClassPrefab : MonoBehaviour
{

    [SerializeField]
    GameObject classTextGO;
    GameObject classTextClone;

    static Transform packageParent;

    int maxNumClassesRow = 3;
    float classOffSet = 2.0f;

    //Counter of classes being drawn
    public static int ClassCounter { get; set; } = 0;

    //Holds the position of where the new line of classes should begin
    //static Vector3 newLinePosition;
    static Vector3 nextClassPos;
    
    static float classNewLineOffset = 0.0f, maxClassHeight = 0.0f;

    void Awake() {

        if (ClassCounter == 0) {
            classNewLineOffset = 0.0f;
            maxClassHeight = 0.0f;
            packageParent = transform.parent;
            nextClassPos = packageParent.position 
                        - new Vector3(0, classOffSet, 0);
        }
        
        ClassCounter++;
        transform.parent = null;

    }
    public void Configure(ClassContainer clazz) {

        float classWidth = clazz.ClassMetrics["NEC"];
        float classHeight = (float)clazz.ClassMetrics["LOC"] / clazz.CHN;
        //Class size
        transform.localScale = new Vector3(classWidth,classHeight,0);
        
        //Set the position for the current class
        transform.position = nextClassPos;

        if (classHeight > maxClassHeight)
            maxClassHeight = classHeight;

        //Define next class position
        if (ClassCounter % maxNumClassesRow != 0) {
            nextClassPos += new Vector3(transform.localScale.x + classOffSet, 0, 0);
        } else {
            classNewLineOffset += maxClassHeight;
            nextClassPos = packageParent.position - new Vector3(0, classNewLineOffset + 2*classOffSet, 0);
        }

        //Class name
        Vector3 textPosition = (Vector3.up + transform.position);
        classTextClone = Instantiate(classTextGO, textPosition, Quaternion.identity);
        int lastDotIndex = clazz.Name.LastIndexOf(".");
        classTextClone.GetComponentInChildren<Text>().text = clazz.Name.Substring(lastDotIndex+1);

        transform.parent = packageParent;
    }
}