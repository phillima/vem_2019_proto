﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PackagePrefab : MonoBehaviour
{

    [SerializeField]
    GameObject packageTextGO;
    GameObject packageTextClone;

    public float PackageHeight { get; set; }

    int numClassesHorizon = 3; //Number of classes being drawn horizontally

    public void Configure(string packageName, List<ClassContainer> classContainers, float CHN) {
        List<int> numNecValues = new List<int>();
        int classOffset = 2; //Distance(horizontally and vertically) between classes being draw
        float maxClassHeight = 0;
        float packageWidth = 0, packageWidthTemp = 0;
        int classHorizontalCounter = 0;
        foreach (var clazz in classContainers) {
            numNecValues.Add(clazz.ClassMetrics["NEC"]);
            //Obtain greatest class height for the package
            if (((float)clazz.ClassMetrics["LOC"] / CHN) > maxClassHeight)
                maxClassHeight = (float)clazz.ClassMetrics["LOC"] / CHN;

            //Calculating packagewidth
            packageWidthTemp += (clazz.ClassMetrics["NEC"] + classOffset);
            classHorizontalCounter++;
            if(classHorizontalCounter >= numClassesHorizon) {
                classHorizontalCounter = 0;
                if (packageWidthTemp > packageWidth) {
                    packageWidth = packageWidthTemp;
                    packageWidthTemp = 0;
                }
            } 
            
        }
        if (packageWidthTemp > packageWidth)
            packageWidth = packageWidthTemp;
        packageWidth += classOffset; //extra offset
        
        //Package Height
        float numClassesVerticaly = Mathf.Ceil((float)classContainers.Count / (float)numClassesHorizon);
        PackageHeight = numClassesVerticaly * (maxClassHeight + classOffset);
        //Define package size
        transform.localScale = new Vector3(packageWidth,PackageHeight,0);

        //Package name
        Vector3 textPosition = (Vector3.up + transform.position);
        packageTextClone = Instantiate(packageTextGO, textPosition, Quaternion.identity);
        packageTextClone.GetComponentInChildren<Text>().text = packageName;
    }
}
