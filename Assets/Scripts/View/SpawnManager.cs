﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    ProjectContainer projectContainer;

    [SerializeField]
    GameObject classPrefab;
    [SerializeField]
    GameObject packagePrefab;
    [SerializeField]
    GameObject annotPrefab;

    AVisualizer aVisualizer;

    void Start() {
        aVisualizer = FindObjectOfType<AVisualizer>();
        projectContainer = aVisualizer.ProjectContainer;
        SpawnProject();
    }

    public void SpawnProject() {
        //Draw packages
        Vector3 packagePos = new Vector3(0, 0, 0);
        foreach (PackageContainer package in projectContainer.PackageContainers) {
            GameObject packageClone = Instantiate(packagePrefab, packagePos, Quaternion.identity);
            packageClone.GetComponent<PackagePrefab>().Configure(
                                                package.Name,
                                                package.ClassContainer, projectContainer.CHN);
            packagePos = packageClone.transform.position - new Vector3(0, packageClone.transform.localScale.y + 2, 0);
            SpawnClasses(packageClone, package.ClassContainer);
        }
    }

    void SpawnClasses(GameObject packageGO, List<ClassContainer> classContainers) {

        Vector3 classPos = packageGO.transform.position;
        classContainers.ForEach(AddCHN);
        classContainers.Sort();
        foreach (ClassContainer clazz in classContainers) {
            GameObject clazzClone = Instantiate(classPrefab, classPos,
                        Quaternion.identity, packageGO.transform);
            clazzClone.GetComponent<ClassPrefab>().Configure(clazz);
            SpawnAnnotations(clazzClone, clazz.ElementCointainers);
        }
        ClassPrefab.ClassCounter = 0;//resets the counter for the classes of the next package
    }

    void SpawnAnnotations(GameObject clazzClone, 
                    Dictionary<string, ElementContainer> elementCointainers) {
        List<ElementContainer> elementList = 
                new List<ElementContainer>(elementCointainers.Values);
        elementList.Sort();

        foreach (ElementContainer elementContainer in elementList) {
            foreach (AnnotationContainer annotation in elementContainer.Annotations) {
                GameObject annotationClone = Instantiate(annotPrefab, clazzClone.transform);
                annotationClone.GetComponent<AnnotationPrefab>().
                            Configure(annotation,projectContainer.Max_AA);
            }
            AnnotationPrefab.RowCounter++;
            AnnotationPrefab.NewRow = true;
        }
        AnnotationPrefab.RowCounter = 0;
        AnnotationPrefab.AnnotationCounter = 0;
        AnnotationPrefab.NewRow = true;
    }

    //Inner helper methods
    void AddCHN(ClassContainer clazz){
        clazz.CHN = projectContainer.CHN;
    }
}